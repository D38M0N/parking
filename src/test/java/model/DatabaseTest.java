package model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import view.ScannerMVC;

import javax.print.attribute.standard.DateTimeAtCompleted;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class DatabaseTest {

    Database instance = Database.getInstance();
    ScannerMVC.Controller scanner = Mockito.mock(ScannerMVC.Controller.class);


    @Test
    void getInstance() {
        Assert.assertTrue(instance.getTicketDatabaseDAO().getSpots().size() == 10);
        Assert.assertTrue(instance != null);
    }

    @Test
    void chooseTicket() {
        Assert.assertFalse(instance.getTicketDatabaseDAO().getTickets().size() == 1);

        instance.chooseTicket(new Motorcycle());
        instance.chooseTicket(new TruckCar());
        Assert.assertTrue(instance.getTicketDatabaseDAO().getTickets().size() == 2);
        Assert.assertTrue(instance.getTicketDatabaseDAO().getTickets().get(0).getTicketID() == 1);
        Assert.assertTrue(instance.getTicketDatabaseDAO().getTickets().get(1).getTicketID() == 2);
        Assert.assertTrue(instance.getTicketDatabaseDAO().getTickets().get(0).getSpotID() == 1);
        Assert.assertTrue(instance.getTicketDatabaseDAO().getTickets().get(0).getCostForPark() == 0.0);
        Assert.assertTrue(instance.getTicketDatabaseDAO().getTickets().get(0).getVehicle().getPrince() == 5);
    }

    @Test
    void showAllTicket() {
        instance.chooseTicket(new Motorcycle());
        instance.chooseTicket(new TruckCar());
        instance.showAllTicket();
        Assert.assertTrue(instance.getTicketDatabaseDAO().getTickets().size() == 2);
    }


    @Test
    void endParking() {

        instance.chooseTicket(new Motorcycle());
        instance.chooseTicket(new TruckCar());

        Mockito.when(scanner.pickOption()).thenReturn(1);
        instance.setScanner(scanner);

        instance.endParking();

        Assert.assertTrue(instance.getTicketDatabaseDAO().getHistoryParking().get(0).getCostForPark() == 5.0);
        Assert.assertTrue(instance.getTicketDatabaseDAO().getHistoryParking().size() == 1);

    }

}