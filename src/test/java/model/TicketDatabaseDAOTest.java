package model;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TicketDatabaseDAOTest {
    TicketDAO ticketDAO = new TicketDatabaseDAO();
    Vehicle vehicle = new Motorcycle();

    @Test
    void factoryTicket() throws InterruptedException {
        ticketDAO.createSpots(10);
        Assert.assertTrue(ticketDAO.getSpots().size() == 10);

        ticketDAO.factoryTicket(vehicle);
        ticketDAO.factoryTicket(vehicle);
        ticketDAO.factoryTicket(vehicle);

        Assert.assertTrue(ticketDAO.getTickets().size() == 3);
        Assert.assertFalse(ticketDAO.getSpots().get(1).isFree());

        Thread.sleep(4000);
        ticketDAO.endParking(2);

        Assert.assertTrue(ticketDAO.getSpots().get(1).isFree());
        Assert.assertTrue(ticketDAO.getTickets().size() == 2);
        Assert.assertTrue(ticketDAO.getHistoryParking().size() == 1);
        Assert.assertTrue(20 == ticketDAO.getHistoryParking().get(0).getCostForPark());




    }


}