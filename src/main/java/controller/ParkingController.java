package controller;

import model.Database;
import model.VehicleFactory;
import view.ParkingMVC;
import view.ParkingView;
import view.ScannerMVC;

import java.time.LocalDate;

public class ParkingController implements ParkingMVC.Controller {
    private ScannerMVC.Controller scannerController;

    public ParkingController() {
        scannerController = new ScannerController();
    }

    @Override
    public void attach(ParkingMVC.View view) {

        view.showWelcome();
        int option = scannerController.pickOption();
        switch (option) {
            case 1:
                view.showMenuTicket();
                Database.getInstance().chooseTicket(VehicleFactory.getVehicle());
                attach(view);
                break;
            case 2:
                view.showFreeSpot();
                attach(view);
                break;
            case 3:
                view.showCurrentVehicleOnParking();
                attach(view);
                break;
            case 4:
                view.showYourTicket();
                attach(view);
                break;
            case 5:
                view.showYourTicket();
                view.askPick();
                Database.getInstance().endParking();
                attach(view);
                break;
            case 6:
                view.dailyStatement(LocalDate.now());
                attach(view);
                break;

            case 0:
                System.exit(0);
            default:
                attach(view);
                break;
        }


    }

}

