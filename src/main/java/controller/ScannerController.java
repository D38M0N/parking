package controller;

import view.ScannerMVC;

import java.util.Scanner;

public class ScannerController implements ScannerMVC.Controller {

    @Override
    public int nextInt() {
        try {
            return new Scanner(System.in).nextInt();
        } catch (Exception e) {
            System.out.println("Upss... try again ");
            return nextInt();
        }
    }

    @Override
    public int pickOption() {
        try {
        return new Scanner(System.in).nextInt();
    } catch (Exception e) {
        System.out.println("Upss... try again ");
        return pickOption();
    }

}


}
