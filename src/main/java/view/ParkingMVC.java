package view;

import java.time.LocalDate;
import java.time.LocalDateTime;

public interface ParkingMVC {
    interface View {
        void showWelcome();

        void showMenuTicket();

        void showYourTicket();

        void showCurrentVehicleOnParking();

        void showFreeSpot();

        void dailyStatement(LocalDate date);

        void askPick();


    }

    interface Controller {

        void attach(View view);
    }
}
