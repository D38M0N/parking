package view;

import controller.ParkingController;
import model.Database;
import view.ParkingMVC.View;

import java.time.LocalDate;

public class ParkingView implements View {
    public ParkingView() throws InterruptedException {
        System.out.println("----------->>> Welcome to parking by D38M0N <<<-----------" +
                "\n\n To day is  " + LocalDate.now() + '\n');
        loading();

        ParkingMVC.Controller parkingController = new ParkingController();
        parkingController.attach(this);

    }

    private void loading() throws InterruptedException {
        int i = 0;
        System.out.println("|  LOADING MENU |");
        System.out.print("|");
        while (i < 15) {
            System.out.print("=");
            Thread.sleep(250);
            i++;
        }
        System.out.println("| 100 %");
    }

    @Override
    public void showWelcome() {
        System.out.println("\n    *** MENU ***" +
                "\n 1. Park up " +
                "\n 2. List free spot " +
                "\n 3. Vehicle on parking " +
                "\n 4. Show all ticket" +
                "\n 5. Pay by ticket" +
                "\n 6. Daily statement" +
                "\n 0. End Program");

    }

    @Override
    public void showMenuTicket() {
        System.out.println("\n ---- Pleas chose your vehicle ----");
        System.out.println("1. Motorcycle ");
        System.out.println("2. Passenger Car ");
        System.out.println("3. Truck ");
    }

    @Override
    public void showYourTicket() {
        System.out.println("\n ---- Your ticket  ---- ");
        Database.getInstance().showAllTicket();
    }

    @Override
    public void showCurrentVehicleOnParking() {
        System.out.println("\n ---- Vehicle on parking ---- ");
        Database.getInstance().vehicleOnParking();
    }

    @Override
    public void showFreeSpot() {
        System.out.println("\n ---- List free spot: ---- ");
        Database.getInstance().showFreeSpot();
    }

    @Override
    public void dailyStatement(LocalDate date) {
        System.out.println("\n ---- Daily statement: ---- ");
        Database.getInstance().reportForDay(date);

    }

    @Override
    public void askPick() {
        System.out.println("Chose your ticket");
    }
}
