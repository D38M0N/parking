package model;

public class TruckCar extends Vehicle {
    private final int prince = 15;


    @Override
    public String toString() {
        return "Truck Car: " +
                "\n     Prince: " + prince;
    }

    @Override
    public int getPrince() {

        return prince;
    }
}
