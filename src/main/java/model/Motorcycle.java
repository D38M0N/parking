package model;

public class Motorcycle extends Vehicle {
    private final int prince = 5;

    @Override
    public String toString() {
        return "Motorcycle " +
                "\n     Prince: " + prince;
    }

    @Override
    public int getPrince() {
        return prince;
    }
}
