package model;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Ticket {
    private Vehicle vehicle;
    private int ticketID;
    private int spotID;
    private LocalDateTime startParkTime;
    private LocalDateTime stopParkTime;

    public Vehicle getVehicle() {
        return vehicle;
    }

    public LocalDate getEndParkingDate() {

        return stopParkTime.toLocalDate();
    }

    public int getSpotID() {
        return spotID;
    }

    public double getCostForPark() {
        if (stopParkTime == null) {
            return Duration.between(startParkTime, LocalDateTime.now()).getSeconds() * vehicle.getPrince() + vehicle.getPrince();
        } else {
            return Duration.between(startParkTime, stopParkTime).getSeconds() * vehicle.getPrince() + vehicle.getPrince();
        }
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public void setStartParkTime(LocalDateTime startParkTime) {
        this.startParkTime = startParkTime;
    }

    public void setStopParkTime(LocalDateTime stopParkTime) {
        this.stopParkTime = stopParkTime;
    }

    public int getTicketID() {
        return ticketID;
    }

    public void setTicketID(int ticketID) {
        this.ticketID = ticketID;
    }

    public void setSpotID(int spotID) {
        this.spotID = spotID;
    }


    public String nameAndSpot() {
        return "\nTyp: " + vehicle.toString() + "\n Place: " + spotID;
    }

    @Override
    public String toString() {
        if (stopParkTime != null) {

            return "\n- Ticket number: " + ticketID +
                    "\n     Type: " + vehicle.toString() +
                    "\n     Spot number: " + spotID +
                    "\n     Start parking: " + startParkTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss ")) +
                    "\n     Stop parking: " + stopParkTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss ")) +
                    "\n     Cost parking: " + getCostForPark();
        } else {

            return "\n- Ticket number: " + ticketID +
                    "\n     Type: " + vehicle.toString() +
                    "\n     Spot number: " + spotID +
                    "\n     Start parking: " + startParkTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss ")) +
                    "\n     Stop parking: - - - " +
                    "\n     Current cost parking: " + getCostForPark();
        }

    }
}
