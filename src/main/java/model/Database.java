package model;

import controller.ScannerController;
import view.ScannerMVC;

import java.time.LocalDate;
import java.util.List;

public class Database {
    private static Database instance;
    private static int limitSpots = 10;

    public TicketDatabaseDAO getTicketDatabaseDAO() {
        return ticketDatabaseDAO;
    }

    private TicketDatabaseDAO ticketDatabaseDAO;
    private ScannerMVC.Controller scanner = new ScannerController();

    public void setScanner(ScannerMVC.Controller scanner) {
        this.scanner = scanner;
    }

    public Database() {
    }

    public static Database getInstance() {
        if (instance == null) {
            instance = new Database();
            instance.ticketDatabaseDAO = new TicketDatabaseDAO();
            instance.ticketDatabaseDAO.createSpots(limitSpots);
        }
        return instance;
    }


    public void chooseTicket(Vehicle vehicle) {
        instance.ticketDatabaseDAO.factoryTicket(vehicle);

    }

    public void showAllTicket() {
        for (Ticket ticket : instance.ticketDatabaseDAO.getTickets()) {
            System.out.println(ticket.toString());
        }
    }

    public void endParking() {
        if (Database.getInstance().ticketDatabaseDAO.getTickets().isEmpty()) {
            System.out.println("Is Empty");
        } else {

            Database.getInstance().ticketDatabaseDAO.endParking(scanner.pickOption());
        }
    }

    public void vehicleOnParking() {
        for (Ticket ticket : Database.getInstance().ticketDatabaseDAO.getTickets()) {
            System.out.println(ticket.nameAndSpot());
        }
    }

    public void showFreeSpot() {
        for (Spot spot : Database.getInstance().ticketDatabaseDAO.getSpots()) {
            if (spot.isFree()) System.out.println(spot.toString());
        }
    }

    public void reportForDay(LocalDate date) {

        double moneyByAllDay = 0;

        for (Ticket ticket : getHistoryParking()) {
            if (ticket.getEndParkingDate().equals(date)) {
                System.out.println(ticket.toString());
                moneyByAllDay += ticket.getCostForPark();
            }
        }

        System.out.println("\n ===========> Profit of the day: " + moneyByAllDay +
                "\n Now on park is: " + amountCurrentlyVehiclesOnParking());
    }

    private List<Ticket> getHistoryParking() {
        return Database.getInstance().ticketDatabaseDAO.getHistoryParking();
    }

    private int amountCurrentlyVehiclesOnParking() {
        return Database.getInstance().ticketDatabaseDAO.getTickets().size();
    }

}
