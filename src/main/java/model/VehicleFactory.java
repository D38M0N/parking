package model;

import controller.ScannerController;
import model.Motorcycle;
import model.PassengerCar;
import model.TruckCar;
import model.Vehicle;
import view.ScannerMVC;

import java.util.ArrayList;
import java.util.List;

public class VehicleFactory {

    public static Vehicle getVehicle() {
        Vehicle vehicle = null;
        ScannerMVC.Controller scannerController = new ScannerController();
        switch (scannerController.nextInt()) {
            case 1:
                vehicle = new Motorcycle();
                return vehicle;
            case 2:
                vehicle = new PassengerCar();
                return vehicle;
            case 3:
                vehicle = new TruckCar();
                return vehicle;
            default:
                System.out.println("Try again");
                return getVehicle();
        }

    }


}
