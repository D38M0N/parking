package model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class TicketDatabaseDAO implements TicketDAO {
    private List<Ticket> tickets = new ArrayList<>();
    private List<Spot> spots = new ArrayList<>();

    public List<Ticket> getTickets() {
        return tickets;
    }

    public List<Spot> getSpots() {
        return spots;
    }

    private List<Ticket> historyParking = new ArrayList<>();

    public List<Ticket> getHistoryParking() {
        return historyParking;
    }

    @Override
    public void factoryTicket(Vehicle vehicle) {
        int spotNumber = lookFreeSpot();

        if (spotNumber > 0) {
            Ticket ticket = new Ticket();
            createTicketID(ticket);
            ticket.setVehicle(vehicle);
            ticket.setStartParkTime(LocalDateTime.now());
            ticket.setSpotID(spotNumber);
            tickets.add(ticket);
        } else {
            System.out.println("Parking is busy");
        }
    }

    @Override
    public void endParking(int numberTicket) {
        int i = 0;
        if (tickets.isEmpty()) {

        } else {
            for (Ticket ticket : tickets) {

                if (ticket.getTicketID() == numberTicket) {
                    ticket.setStopParkTime(LocalDateTime.now());
                    removeSpot(ticket.getSpotID());
                    historyParking.add(ticket);
                    System.out.println("You pay: " + ticket.getCostForPark());
                    break;
                }
                i++;
            }
            try {
                tickets.remove(i);
            } catch (Exception e) {
                System.out.println("Upss... try again ");
            }
        }
    }

    private void removeSpot(int spotID) {
        for (Spot spot : spots) {
            if (spot.getNumberOfPosition() == spotID) spot.setFree(true);
        }
    }

    private int lookFreeSpot() {
        for (Spot spot : spots) {
            if (spot.isFree()) {
                spot.setFree(false);
                return spot.getNumberOfPosition();
            }
        }
        return -1;
    }


    private void createTicketID(Ticket ticket) {

        if (tickets.isEmpty()) {
            ticket.setTicketID(1);
        } else {
            int numberCreatorID = tickets.get(tickets.size() - 1).getTicketID() + 1;
            ticket.setTicketID(numberCreatorID);
        }
    }

    public void createSpots(int limitSpots) {
        for (int i = 1; i <= limitSpots; i++) {
            spots.add(new Spot(i));
        }
    }

}
