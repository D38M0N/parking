package model;

public class PassengerCar extends Vehicle {
    private final int prince = 10;

    @Override
    public String toString() {
        return "Passenger Car " +
                "\n     Prince: " + prince;
    }

    @Override
    public int getPrince() {
        return prince;
    }
}
