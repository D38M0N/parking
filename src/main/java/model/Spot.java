package model;

public class Spot {
    private int numberOfPosition;
    private boolean isFree = true;

    public Spot(int numberOfPosition) {
        this.numberOfPosition = numberOfPosition;
    }

    public boolean isFree() {
        return isFree;
    }

    public void setFree(boolean free) {
        isFree = free;
    }


    public int getNumberOfPosition() {
        return numberOfPosition;
    }


    @Override
    public String toString() {
        if (isFree) {
            return numberOfPosition + ". is Free spot";
        } else {
            return numberOfPosition + ". is busy";
        }
    }
}
