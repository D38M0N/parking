package model;

import java.util.List;

public interface TicketDAO {
    void factoryTicket(Vehicle vehicle);

    void endParking(int numberTicket);

    void createSpots(int limitSpots);

    List<Ticket> getHistoryParking();

    List<Ticket> getTickets();

    List<Spot> getSpots();
}
